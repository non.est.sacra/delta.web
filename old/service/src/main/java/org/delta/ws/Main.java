/*
*/

package org.delta.ws;


import zoomba.lang.core.operations.ZRandom;

import static spark.Spark.*;

/**
 */
public class Main {

    public static final String INVALID_SID = "INVALID SID PASSED!!!" ;

    public static final String DANGEROUS_COMMAND = "!!! Sorry, Can not let you do that!!!" ;


    public static String serve(String sessionId, String script){
        try {
            DeltaSession deltaSession = DeltaSession.getSession(sessionId);
            String result = deltaSession.executeScript(script);
            return result;
        }catch (Exception e){
            return e.getMessage();
        }
    }

    public static String remoteBS(String sessionId, String url, String script, String config){
        try {
            DeltaBSSession deltaBSSession = DeltaBSSession.getSession(sessionId);
            String result = deltaBSSession.executeBSScript(url, script, config );
            return result;
        }catch (Exception e){
            return e.getMessage();
        }
    }


    public static boolean inValidIP(String url){
        if ( url == null ) return  true ;
        String[] arr = url.split("/");
        arr = arr[2].split(":");
        return ! ("localhost".equalsIgnoreCase( arr[0] ) );
    }

    public static boolean inValidSessionID(String sid){
        if ( sid == null ) return  true ;
        if ( sid.length() < 16 ) return true ;
        String[] arr = sid.split("\\-");
        if ( arr.length <= 3 ) return true;
        return false;
    }

    public static boolean potentialThreat(String script){
        if ( script == null ) return false ;
        if ( script.contains("system") ) return true;
        if ( script.contains("thread") ) return true;

        return false;
    }

    public static void main(String[] args) {
        // run the scavenger threads
        DeltaSession.gc();
        DeltaBSSession.gc();

        get("/*", (request, response) -> {

            // nothing if not from local host ...
            if ( inValidIP( request.url() ) ) return "" ;

            String uri = request.uri().substring(1);
            int i = uri.indexOf("/");
            if ( i < 0 ) return INVALID_SID ;
            String sid = uri.substring(0,i);
            if ( inValidSessionID(sid) ) return INVALID_SID ;

            String script = uri.substring(i+1);
            // decode base 64
            script = ZRandom.hash("d64", script);
            if ( potentialThreat(script) ) return DANGEROUS_COMMAND ;
            String result = serve(sid,script);
            return result;
        });

        post("/*", (request, response) -> {

            // nothing if not from local host ...
            if ( inValidIP( request.url() ) ) return "" ;
            String sid = request.queryParams("sid");
            String script = request.queryParams("bss"); // BrowserStack Selenium Script
            String url = request.queryParams("tgt"); //  Target URL
            String config = request.queryParams("bsc"); //  BrowserStack Config
            // decode base 64
            script = ZRandom.hash("d64", script);
            url = ZRandom.hash("d64", url);
            config = ZRandom.hash("d64", config );
            String result = remoteBS(sid,url, script, config );
            return result;
        });
    }
}
