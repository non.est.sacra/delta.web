/*
*/

package org.delta.ws;

import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

/**
 */
public class DeltaSession implements Closeable {

    // mark for 15 minutes, good time?
    public static final long SESSION_TIME_OUT_MS = 15 * 60 * 1000 ;

    // mark for 30 secs, enough for people?
    public static final long COMMAND_TIME_OUT_MS = 30 * 1000 ;

    private static final ConcurrentHashMap<String,DeltaSession> deltaSessions = new ConcurrentHashMap<>();

    public static void gc(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        Thread.sleep(30000);
                    }catch (Exception e){

                    }
                    long curTime = System.currentTimeMillis();
                    HashSet<String> sessions = new HashSet(deltaSessions.keySet());
                    for ( String sid : sessions ){
                        DeltaSession deltaSession = deltaSessions.get(sid);
                        if ( curTime - deltaSession.lastAccessTime > SESSION_TIME_OUT_MS){
                            System.err.printf("Timeout on session %s\n", deltaSession.id );
                            deltaSession.destroySession();
                            deltaSessions.remove(sid);
                        }
                    }
                }
            }
        });
        t.start();
    }

    public static DeltaSession getSession(String id) throws Exception {
        DeltaSession deltaSession;
        if ( !deltaSessions.containsKey( id ) ){
            deltaSession = new DeltaSession(id);
            deltaSessions.put(id,deltaSession);
        }
        deltaSession = deltaSessions.get(id);
        deltaSession.lastAccessTime = System.currentTimeMillis();
        return deltaSession;
    }

    public final String id;

    Function.MonadicContainer result ;

    ZContext.FunctionContext sessionContext;

    long lastAccessTime ;


    public DeltaSession(String id) throws Exception {
        this.id = id;
        result = null;
        sessionContext = new ZContext.FunctionContext(
                ZContext.EMPTY_CONTEXT, ZContext.ArgContext.EMPTY_ARGS_CONTEXT );
    }

    public class DeltaTask implements Runnable{

        public final String script;

        String strResult;

        public DeltaTask(String script){
            this.script = script ;
            strResult = "" ;
        }
        @Override
        public void run() {
            PrintStream err = System.err ;
            PrintStream out = System.out ;

            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                PrintStream oe = new PrintStream(baos);
                ZScript zScript = new ZScript( script );
                System.setErr(oe);
                System.setOut(oe);
                // share session context
                zScript.runContext(sessionContext);
                result = zScript.execute();
                String outString = baos.toString();
                String retString = "";
                if ( !outString.isEmpty() ){
                    retString = outString + "\n" ;
                }
                if ( !result.isNil() ) {
                    Object v = result.value();
                    String c = ( v == null) ? "null" : v.getClass().getName();
                    retString += ZTypes.string( v ) + " // " + c  ;
                }
                oe.close();
                strResult = retString ;
                return;
            }catch (Throwable throwable){
                result = new ZException.MonadicException( throwable );
                if ( throwable instanceof ZException){
                    strResult = throwable.toString() ;
                }else {
                    strResult = throwable.getMessage();
                }
            }finally {
                System.setErr(err);
                System.setOut(out);
            }
        }
    }

    Runtime runtime = Runtime.getRuntime();

    public synchronized String executeScript(String script){
        // hook for gc...
        if ( script.startsWith("--gc ")){
            String res = String.format("Mem free before : %d\n", runtime.freeMemory() );
            runtime.gc();
            res += String.format("Mem free after : %d\n", runtime.freeMemory() );
            return res;
        }

        DeltaTask task = new DeltaTask(script);
        Thread t = new Thread( task );
        t.start();
        long throttleTime = System.currentTimeMillis() + COMMAND_TIME_OUT_MS ;
        while( t.isAlive() ){
            long now = System.currentTimeMillis();
            if ( now > throttleTime ){
                t.stop();
                return "Command Timeout Occurred!";
            }
            try {
                Thread.sleep(500);
            }catch (Exception e){}
        }
        return task.strResult ;
    }

    @Override
    public void close() throws IOException {
        destroySession();
    }

    public void destroySession(){
        result = null;
        System.gc();
        sessionContext.clear();
    }
}
