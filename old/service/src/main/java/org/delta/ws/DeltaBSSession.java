/*
*
*/

package org.delta.ws;

import zoomba.lang.core.io.ZFileSystem;
import zoomba.lang.core.types.ZException;
import java.io.*;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

/**
 */
public class DeltaBSSession implements Closeable {

    // DELTA home
    public static final String DELTA_HOME = "/Codes/java/delta/target/" ;

    // DELTA binary
    public static final String DELTA_TESTING = "delta.core-0.1-SNAPSHOT.jar" ;

    // mark for 15 minutes, good time?
    public static final long SESSION_TIME_OUT_MS = 15 * 60 * 1000 ;

    // mark for 30 secs, enough for people?
    public static final long COMMAND_TIME_OUT_MS = 30 * 1000 ;

    private static final ConcurrentHashMap<String,DeltaBSSession> deltaBSSessions = new ConcurrentHashMap<>();

    public static void gc(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        Thread.sleep(30000);
                    }catch (Exception e){

                    }
                    long curTime = System.currentTimeMillis();
                    HashSet<String> sessions = new HashSet(deltaBSSessions.keySet());
                    for ( String sid : sessions ){
                        DeltaBSSession deltaBSSession = deltaBSSessions.get(sid);
                        if ( curTime - deltaBSSession.lastAccessTime > SESSION_TIME_OUT_MS){
                            System.err.printf("Timeout on session %s\n", deltaBSSession.id );
                            deltaBSSession.destroySession();
                            deltaBSSessions.remove(sid);
                        }
                    }
                }
            }
        });
        t.start();
    }

    public static DeltaBSSession getSession(String id) throws Exception {
        DeltaBSSession deltaBSSession;
        if ( !deltaBSSessions.containsKey( id ) ){
            deltaBSSession = new DeltaBSSession(id);
            deltaBSSessions.put(id,deltaBSSession);
        }
        deltaBSSession = deltaBSSessions.get(id);
        deltaBSSession.lastAccessTime = System.currentTimeMillis();
        return deltaBSSession;
    }

    public final String id;

    long lastAccessTime ;

    public DeltaBSSession(String id) throws Exception {
        this.id = id;
    }

    public static final String BS_SESSION_FOLDER = "BS_SESSIONS" ;


    static {
        File f = new File(BS_SESSION_FOLDER);
        if ( !f.exists()){
            try{
               boolean created = f.mkdir();
               assert created ;
            }catch (Exception e){
                throw new RuntimeException(e);
            }
        }
    }

    public class DeltaBSTask implements Runnable{

        public final String sessionId;

        public final String script;

        public final String url;

        public final String config;

        public final File scriptFile;

        public final File configFile;

        public final File outFile;

        Process p;

        String strResult;

        public DeltaBSTask(String sessionId, String url, String script, String bsConfig){
            this.sessionId = sessionId ;
            this.url = url  ;
            this.script = script ;
            this.config = bsConfig ;
            this.scriptFile = new File(BS_SESSION_FOLDER + "/" + sessionId + "-script.jxl");
            this.configFile = new File(BS_SESSION_FOLDER + "/" + sessionId + "-config.json" );
            this.outFile = new File(BS_SESSION_FOLDER + "/" + sessionId + "-oe.txt" );
        }

        @Override
        public void run() {

            try {
                // drop script and config to files
                ZFileSystem.write( scriptFile.getAbsolutePath() , this.script );
                ZFileSystem.write( configFile.getAbsolutePath() , this.config );
                ProcessBuilder pb = new ProcessBuilder();
                pb.command("java", "-jar", DELTA_HOME + DELTA_TESTING,
                        "-u" , this.url , "-bc", configFile.getAbsolutePath() ,
                        scriptFile.getAbsolutePath() );

                pb.redirectOutput(outFile);
                pb.redirectError(outFile);
                p = pb.start();
                p.waitFor();
                strResult = (String) ZFileSystem.read(outFile.getAbsolutePath());
            }catch (Throwable throwable){
                if ( throwable instanceof ZException){
                    strResult =  throwable.toString() ;
                }else {
                    strResult = throwable.getMessage();
                }
            }finally {
                p.destroyForcibly();
            }
        }
    }

    public synchronized String executeBSScript(String url, String script, String config){

        DeltaBSTask task = new DeltaBSTask(this.id, url,script,config);
        Thread t = new Thread( task );
        t.start();
        long throttleTime = System.currentTimeMillis() + COMMAND_TIME_OUT_MS ;
        while( t.isAlive() ){
            long now = System.currentTimeMillis();
            if ( now > throttleTime ){
                t.stop();
                return "Command Timeout Occurred!";
            }
            try {
                Thread.sleep(500);
            }catch (Exception e){}
        }
        return task.strResult ;
    }

    @Override
    public void close() throws IOException {
        destroySession();
    }

    public void destroySession(){
        System.gc();
    }
}
