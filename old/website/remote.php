<?php 

function exec_ZoomBA( $session_id,  $script_text ) {
    $payload = file_get_contents('http://localhost:4567/' . $session_id . "/" . $script_text );
    return $payload ;
} 

function run_bs_ZoomBA( $session_id,  $script_text , $target ,$configuration ) {
    $url = 'http://localhost:4567/';
    $myvars = 'sid=' . $session_id . '&bss=' . $script_text . 
              '&tgt=' . $target . '&bsc=' . $configuration ;
    $ch = curl_init( $url );
    curl_setopt( $ch, CURLOPT_POST, 1);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $myvars);
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt( $ch, CURLOPT_HEADER, 0);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec( $ch );
    return $response ;
}

// get session 
$session = $_POST["s"] ; 
// get executable 
$script = $_POST["e"] ;
// get url 
$tgt = $_POST["u"] ;
// get configuration 
$config = $_POST["c"];

if ( $tgt == null ){
    // call function to forward ZoomBA output to client 
    echo exec_ZoomBA( $session , $script );
}else{
    echo "I would be calling BrowserStack ... \n"  ;
    echo run_bs_ZoomBA($session, $script , $tgt, $config );
}